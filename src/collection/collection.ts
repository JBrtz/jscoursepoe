export abstract class Collection<T> {
    protected collection: T[] = []

    public add(item: T): void {
        this.collection.push(item);
    }

    public update(item: T): void {}

    public remove(item: T): void {}

    public size(): number {
        return this.collection.length;
    }

    public all(): T[] {
        return this.collection;
    }

}