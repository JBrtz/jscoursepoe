"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.UserCollection = void 0;
var collection_1 = require("./collection");
var UserCollection = /** @class */ (function (_super) {
    __extends(UserCollection, _super);
    function UserCollection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UserCollection.prototype.add = function (user) {
        if (this.collection.findIndex(function (item) { return item.getId() === user.getId(); }) === -1) {
            _super.prototype.add.call(this, user);
        }
    };
    return UserCollection;
}(collection_1.Collection));
exports.UserCollection = UserCollection;
