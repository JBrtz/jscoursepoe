import { User } from "../models/user";
import { Collection } from "./collection";

export class UserCollection extends Collection<User> {

    public add(user: User): void {
        if (this.collection.findIndex((item: User) => item.getId() === user.getId()) === -1) {
            super.add(user);
        }
    }
}