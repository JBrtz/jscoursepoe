"use strict";
exports.__esModule = true;
var user_collection_1 = require("./src/collection/user-collection");
var user_1 = require("./src/models/user");
var Main = /** @class */ (function () {
    function Main() {
        var userCollection = new user_collection_1.UserCollection();
        var user = new user_1.User();
        user.lastName = "Aubert";
        user.firstName = "Jean-Luc";
        user.setId("jlaubert");
        user.setPassword("admin");
        console.log(user);
        userCollection.add(user);
        console.log(userCollection);
    }
    return Main;
}());
// Load Main
new Main();
