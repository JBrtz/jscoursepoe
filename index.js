/**
 * main
 * @author Moi <moi@moi.com>
 * Description du contenu et fonction du code
 * @version 1.0.0
 */

let myName = "";
// Affectation de valeur Julien à la variable myName
myName = "Berthezene";

// Affichage du contenu de la variable myName
console.log(myName); // Expected output : Berthezene

// Changer la valeur de myName par autre chose
myName = "Whatever";
console.log(myName); // Expected output : Whatever

let isMale = true;

let users = [];

let yourName = "Bond";
console.log(yourName);
console.log(`${myName} <=> ${yourName}`);

users = [
    "Aubert",
    "Abdel",
    "Nesibe",
    "Rainui",
    "Remi"
];

for (let i = 0; i < users.length ; i++) {
    console.log(users[i]);
}

console.log();

let indice = 0;
while (indice < users.length) {
    console.log(users[indice]);
    indice += 1;
}



