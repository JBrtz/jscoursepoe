import { UserCollection } from "./src/collection/user-collection";
import {User} from "./src/models/user";

class Main {
    public constructor() {
        const userCollection: UserCollection = new UserCollection();
        const user: User = new User();
        user.lastName = "Aubert";
        user.firstName = "Jean-Luc";
        user.setId("jlaubert");
        user.setPassword("admin");

        userCollection.add(user);
        console.log(userCollection);
    
    }
}

// Load Main
new Main();