// (De)crypt via an inverted alphabet

function encrypt(password) {
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`! @#$%^&*()_-+={[}]|\\:;\"'<,>.?/";
    let negaAlphabet = "";
    for (char of alphabet) {
        negaAlphabet = char + negaAlphabet;
    }
    let encryptedPassword = "";
    for (i = 0; i < password.length; i++) {
        for (j = 0; j < alphabet.length; j++){
            if (password[i] === alphabet[j]) {
                encryptedPassword += negaAlphabet[j];
            }
        }
    }
    return encryptedPassword;
}

let psw = "Hello";
console.log("Password is " + psw);
psw = encrypt(psw);
console.log("Encrypted password is " + psw);
psw = encrypt(psw);
console.log("Decrypted password is " + psw);
console.log()



//This time using a chosen offset value.
//Decrypt by using negative of the offset value. 

function offsetAlphabet(offset) {
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`! @#$%^&*()_-+={[}]|\\:;\"'<,>.?/";
    let encryptedAlphabet = "";
    while (offset > alphabet.length) {
        offset -= alphabet.length; 
    }
    while (offset < 0) {
    offset += alphabet.length;
    }
    for (i = 0; i < alphabet.length; i++) {
        if (i + offset < alphabet.length) {
            encryptedAlphabet += alphabet[i + offset];
        }
        else {
            encryptedAlphabet += alphabet[offset - (alphabet.length - i)];
        }
    }
        return encryptedAlphabet;
}

function offsetPassword(password, chosenEncryptionNumber) {
    let encryptedPassword = "";
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`! @#$%^&*()_-+={[}]|\\:;\"'<,>.?/";
    let newAlphabet = "";
    newAlphabet = offsetAlphabet(chosenEncryptionNumber);
    for (i = 0; i < password.length; i++) {
        for (j = 0; j < alphabet.length; j++){
            if (password[i] === alphabet[j]) {
                encryptedPassword += newAlphabet[j];
            }
        }
    }
    return encryptedPassword;
}

psw2 = "Bonjour";
console.log("Pasword is " + psw2);
psw2 = offsetPassword(psw2, 658);
console.log("Encrypted password is " + psw2);
psw2 = offsetPassword(psw2, -658)
console.log("Decrypted password is " + psw2);