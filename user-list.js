class User {
    constructor() {
        this.name = "";
        this.firstName = "";
        this.id = "";
        this.password = "";
    }
}

/**
 * unique(user: User, list: User[])
 * @param user: User Une variable de type User
 * @param list: User[] un tableau de User
 * @returns boolean 
 */
function unique(user, list) {
    for (anyUser of list) {
        if (user.id === anyUser.id) {
            return false;
        }
    }
    return true;
}

/**
 * 
 * @param {*} user User Une variable de type User
 * @param {*} list User[] un tableau de User
 */
function add(user, list) {
    if (unique(user, list)) {
        list.push(user);
    }
    return list;
}

let listUsers = [];

const user = new User();
user.name = "Aubert";
user.firstName = "Jean-Luc";
user.id = "jlaubert";
user.password = "admin";

listUsers = add(user, listUsers);
listUsers = add(user, listUsers);


console.log(listUsers);